import { StyleSheet, Text, View, Modal, TouchableOpacity, Alert } from 'react-native'
import React, {useState} from 'react'

const ModelPopup = () => {
    const [HideShow, setHideShow] = useState('false')
    const onClickModelPopup = () => {
        setHideShow(!HideShow)
    }
  return (
    <View style={styles.container}>
      <Text style={styles.maintxt}>ModelPopup</Text>
       <Modal
            animationType="slide"
            transparent={true}
            visible={HideShow}
            onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setHideShow(!HideShow);
        }}
      >
      <View style={styles.modelback}>
      <TouchableOpacity onPress={onClickModelPopup}>
      <Text style={styles.closebtn}> X </Text>
      </TouchableOpacity>
      <Text style={styles.header}> Header</Text>
            <Text style={styles.header}>This is Modal Popup</Text>
            <Text style={styles.header}>This is Modal Popup</Text>
            <Text style={styles.header}>This is Modal Popup</Text>
            <Text style={styles.header}>This is Modal Popup</Text>
            <Text style={styles.header}>This is Modal Popup</Text>
            <Text style={styles.header}>This is Modal Popup</Text>
            <Text style={styles.header}>This is Modal Popup</Text>
            <Text style={styles.header}>This is Modal Popup</Text>
      </View>
      </Modal>
      <TouchableOpacity onPress={onClickModelPopup}>
            <Text style={styles.btn}>Click Here</Text>
      </TouchableOpacity>
    </View>
  )
}

export default ModelPopup

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
  },
    maintxt:{
      fontSize: 18,
      fontWeight:'700',
      color: 'brown',
      padding: 20
  },
    btn:{
      fontSize: 18,
      fontWeight: '700',
      padding: 10,
      borderWidth: 1,
      borderColor: 'red',
      backgroundColor: 'gray',
      borderRadius: 10
  },
    closebtn:{
      fontSize: 18,
      fontWeight:'700',
      alignSelf:'flex-end',
      marginRight: 30,
      padding: 20
  },
    modelback:{
      backgroundColor: 'lightyellow',
      marginVertical: 30,
      marginHorizontal: 10,
      borderWidth: 1,
      borderRadius: 8
  },
    header:{
      fontSize: 18,
      fontWeight:'700',
      color: 'brown',
      padding: 20,
      alignSelf:'center'
  }
})