import React, { Component } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import NavigationStrings from '../Constants/NavigationStrings';


const Account = ({navigation}) => {
    const GotoAccount = () =>{
        navigation.navigate(NavigationStrings.EDITACCOUNT)
    } 
    return (
        <View style={styles.container}>
            <Text>Account Components</Text>
            <Button title='Edit Account' onPress={GotoAccount}/>
            <Button title='Edit Profile' onPress={() =>navigation.navigate(NavigationStrings.EDITPROFILE)}/>
            <Button title='Model Popup' onPress={() =>navigation.navigate('ModelPopup')}/>
            <Button title='Map Function' onPress={() =>navigation.navigate('FlatList')}/>
            <Button title='Camera' onPress={() =>navigation.navigate('Camera')}/>
            <Button title='Video' onPress={() =>navigation.navigate('Video')}/>
            <Button title='Notification' onPress={() =>navigation.navigate('Notification')}/>
        </View>
    )
}

export default Account;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});