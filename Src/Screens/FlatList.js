import { Alert, StyleSheet, Text, View, Modal, FlatList } from 'react-native'
import React, {useState} from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { 
  Poppins_100Thin,
  Poppins_100Thin_Italic,
  Poppins_200ExtraLight,
  Poppins_200ExtraLight_Italic,
  Poppins_300Light,
  Poppins_300Light_Italic,
  Poppins_400Regular,
  Poppins_400Regular_Italic,
  Poppins_500Medium,
  Poppins_500Medium_Italic,
  Poppins_600SemiBold,
  Poppins_600SemiBold_Italic,
  Poppins_700Bold,
  Poppins_700Bold_Italic,
  Poppins_800ExtraBold,
  Poppins_800ExtraBold_Italic,
  Poppins_900Black,
  Poppins_900Black_Italic, useFonts } from "@expo-google-fonts/poppins";
import AppLoading from "expo-app-loading";
const Flatlist = () => {
    const data = [
      { id: 1, name: "John Doe", city:"nashik" },
      { id: 2, name: "Victor Wayne", city:"Jalgaon" },
      { id: 3, name: "Jane Doe", city:"malegaon" },
    ];
    const Item = ({ id, name, city }) => (
      <View style={styles.item}>
        <Text style={styles.title}>{id}</Text>
        <Text style={styles.title}>{name}</Text>
        <Text style={styles.title}>{city}</Text>
      </View>
    );
    const renderItem = ({ item }) => (
      <Item id={item.id} name={item.name} city={item.city} />
    );
  let [fontsLoaded] = useFonts({ 
    Poppins_100Thin,
    Poppins_100Thin_Italic,
    Poppins_200ExtraLight,
    Poppins_200ExtraLight_Italic,
    Poppins_300Light,
    Poppins_300Light_Italic,
    Poppins_400Regular,
    Poppins_400Regular_Italic,
    Poppins_500Medium,
    Poppins_500Medium_Italic,
    Poppins_600SemiBold,
    Poppins_600SemiBold_Italic,
    Poppins_700Bold,
    Poppins_700Bold_Italic,
    Poppins_800ExtraBold,
    Poppins_800ExtraBold_Italic,
    Poppins_900Black,
    Poppins_900Black_Italic, });

  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
  return (
    <View style={styles.container}>
      {/* <Text style={styles.maintxt}>MapFunction</Text>
        {data.map((ABC) => (
        <View style={{flexDirection:'row'}}>

        <Text style={styles.header}>{ABC.id}</Text>
        <Text style={styles.header}>{ABC.name}</Text>
        <Text style={styles.header}>{ABC.city}</Text>
        </View>
      ))} */}
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
      
    </View>
  )
}
}
export default Flatlist

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
  },
   maintxt:{
      fontSize: 18,
      fontWeight:'700',
      color: 'brown',
      padding: 20
  },
  btn:{
      fontSize: 18,
      fontWeight: '700',
      padding: 10,
      borderWidth: 1,
      borderColor: 'red',
      backgroundColor: 'gray',
      borderRadius: 10
  },
  closebtn:{
      fontSize: 18,
      fontWeight:'700',
      alignSelf:'flex-end',
      marginRight: 30,
      padding: 20
  },
  modelback:{
      backgroundColor: 'lightyellow',
      marginVertical: 30,
      marginHorizontal: 10,
      borderWidth: 1,
      borderRadius: 8
  },
  header:{
      fontSize: 18,
      fontWeight:'700',
      color: 'brown',
      padding: 20,
      alignSelf:'center'
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    
  },
  title: {
    fontSize: 32,
    fontFamily: 'Poppins_300Light_Italic'
  },
})