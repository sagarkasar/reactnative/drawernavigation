import { StyleSheet, Text, View, ScrollView, Button } from 'react-native'
import React, { useState } from 'react'
import TopTab from '../Navigation/TopTab';
import DateTimePickerModal from "react-native-modal-datetime-picker";
const TabBarScreen = () => {
  
  return (
    <View style={styles.viewpage}>
      <TopTab /> 
    </View>
  )
}

export default TabBarScreen

const styles = StyleSheet.create({
viewpage:{
 
  flex: 1
}
})