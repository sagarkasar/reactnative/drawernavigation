import React, { useState } from 'react'
import { StyleSheet, Text, View, ScrollView, TextInput, TouchableOpacity,  } from 'react-native'
import { Formik } from 'formik';
import * as yup from "yup";


const EditProfile = (values) => {
    const [name, setname] = useState("");
    const [email, setemail] = useState("");
    const [phone, setphone] = useState("");

    const onSubmit = async (values) => {
        console.log("Name " + values.name + 
                    " Email " + values.email + 
                    " Phone " + values.phone);
    }

    return (
        <View style={styles.container}>
        <ScrollView>
         <Formik
          initialValues={{
              name: name,                              
              email: email,
              phone: phone,
          }}
          onSubmit={onSubmit}
          validationSchema={
            yup.object().shape({
              name: yup.string().required(),
              email: yup.string().email().required(),
              phone: yup.number().required().min(10),
          })}>
          {({
              values,
              handleChange,
              errors,
              setFieldTouched,
              touched,
              isValid,
              handleSubmit,
          }) => (
              <View>
                  <Text style={styles.txt}>Name</Text>
                  <TextInput
                      style={styles.inputtxt}
                      placeholder='name'
                      defaultValue={values.name}
                      onChangeText={handleChange('name')}
                      onChange={() => setname(name)}
                      onBlur={() => setFieldTouched('name')}
                  />
                  {touched.name && errors.name && (
                      <Text style={styles.error}>{errors.name}</Text>
                  )}
                  

                  <Text style={styles.txt}> Email</Text>

                  <TextInput
                      style={styles.inputtxt}
                      placeholder=' email'
                      
                      defaultValue={values.email}
                      onChangeText={handleChange('email')}
                      onChange={() => setemail(email)}
                      onBlur={() => setFieldTouched('email')}
                  />
                  {touched.email && errors.email && (
                      <Text style={styles.error}>{errors.email}</Text>
                  )}

                  <Text style={styles.txt}>Phone</Text>
                  <TextInput
                      style={[styles.inputtxt,]}
                      placeholder='phone'
                      
                      defaultValue={values.phone}
                      onChangeText={handleChange('phone')}
                      onChange={() => setphone(phone)}
                      onBlur={() => setFieldTouched('phone')}
                  />
                  {touched.phone && errors.phone && (
                      <Text style={styles.error}>{errors.phone}</Text>
                  )}
                  
                  <TouchableOpacity 
                    onPress={() => handleSubmit()}                                      
                    disabled={!isValid}                                      
                  >
                      <Text style={styles.btn}>Submit</Text>
                  </TouchableOpacity>
              </View>
          )}
      </Formik>
      </ScrollView>
    </View>
    )
}

export default EditProfile

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text:{
    color:'red',
    fontSize:25,
    fontFamily: 'Poppins_700Bold_Italic'
  },
  error:{
    fontSize: 16,
    color: 'red',
    marginTop: 2
  },
  inputtxt:{
    height: 50,
    width: 300,
    borderRadius: 10,
    borderColor: 'green',
    borderWidth: 1,
    padding: 10

  },
  txt:{
    fontSize: 18,
    fontWeight: '700',
  },
  btn:{
    height: 40,
    width: 170,
    backgroundColor: 'gray',
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 1,
    textAlign:'center',
    marginTop: 20,
    padding: 7,
    fontSize: 20  
  }
})
