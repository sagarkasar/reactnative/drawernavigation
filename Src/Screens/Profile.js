import React, { Component } from 'react'
import { StyleSheet, Text, View, ScrollView, TouchableOpacity  } from 'react-native'


const Profile = ({navigation}) => {
    
    return (
          <View>
           
            <TouchableOpacity onPress={()=>navigation.navigate('FlexBoxDesign')}>
                <Text style={styles.btn} >flexBox</Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={styles.btn} onPress={()=>navigation.navigate('TabBarScreen')}>Top Tab</Text>
            </TouchableOpacity>
            
          </View>
    )
  }

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  maintxt:{
    fontSize: 20,
    color: 'brown'
  },
  btn:{
      color: 'black',
      backgroundColor: 'lightblue',
      height: 50,
      width: 300,
      borderRadius: 30,
      borderWidth: 1,
      borderColor: 'red',
      margin:15,
      fontSize: 23,
      padding: 5,
      textAlign: 'center',
      alignSelf:'center'
  }
});