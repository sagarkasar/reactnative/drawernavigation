import React from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import NavigationStrings from '../Constants/NavigationStrings';
import ProductDetails from './ProductDetails';
import Profile from './Profile';

const Home = ({navigation}) => {

    return (
        <View style={styles.container}>
            <Text>Home Components</Text>
            <Button title='Goto Product Details' onPress={() =>navigation.navigate(NavigationStrings.PRODUCTDETAILS)}/>
            <ProductDetails 
              name="Sagar" 
              email="Sagark.supra@gmail.com" 
              address="Nashik" 
              phone="1234567890"
            />
        </View>
    )
}

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
