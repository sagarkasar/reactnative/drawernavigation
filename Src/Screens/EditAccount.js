import React from 'react';
import { StyleSheet, Text, View, ScrollView} from 'react-native';
import { Button, ActivityIndicator, Colors, Appbar, Avatar, Badge, Card, Title, Paragraph, Checkbox, Chip, Divider, FAB } from 'react-native-paper';
import * as Animatable from 'react-native-animatable';
const EditAccount = () => {
    const LeftContent = props => <Avatar.Icon {...props} icon="folder" />
    const [checked, setChecked] = React.useState(true);
   
    return (
        <View style={styles.container}>
        <ScrollView>
        <View >
            <Text style={styles.text}>Edit Account Page</Text>
            <Divider style={{marginVertical:20}}/>
             <Appbar.Header dark={true}>
                <Appbar.Content color='orange' title="Title" subtitle={'Subtitle'} />
                <Appbar.Action icon="magnify" onPress={() => {}} />
              </Appbar.Header>

              <Divider style={{marginVertical:20}}/>
              
            <Button icon="camera" mode="contained" color='lightblue' onPress={() => alert('Pressed')}>
                Press me
            </Button>
             <Button icon="camera" mode="outlined" onPress={() => alert('Pressed')}>
                Press me
            </Button>
            <Button  icon={{ source: { uri: 'https://avatars0.githubusercontent.com/u/17571969?v=3&s=400' }, direction: 'rtl' }}>
                Press Me
            </Button>
            <Button mode="outlined" icon={{ source: { uri: 'https://avatars0.githubusercontent.com/u/17571969?v=3&s=400' }, direction: 'ltr' }}>
                Press Me
            </Button>
            </View>
            <Divider style={{marginVertical:20}}/>
            <View style={{flexDirection:'row'}}>
            <ActivityIndicator animating={true} size='large' color={Colors.red800} />
            <ActivityIndicator animating={true} color={Colors.blue500} />
            <Avatar.Icon size={24} icon="folder" color='yellow' />
            <Avatar.Icon size={35} icon="folder" color='lightblue' />
            <Avatar.Text size={40} label="JD" color='red'/>
            <Badge size={20}>3</Badge>
            <Checkbox
            status={checked ? 'checked' : 'unchecked'}
            onPress={() => {
                setChecked(!checked);
            }}
            color='brown'
            />
            <Chip icon="information" mode='outlined' onPress={() => alert('Pressed')}>Example Chip</Chip>
            </View>
            <View style={{flexDirection:'row'}}>
                <FAB
                    style={styles.fab}
                   
                    icon="plus"
                    onPress={() => alert('Pressed')}
                    
                />
                <FAB
                    style={styles.fab}
                    color='red'
                    icon="close"
                    onPress={() => alert('Pressed')}
                    label='Close'
                    
                />
           </View>
            <Divider style={{marginVertical:20}}/>
            <Animatable.Text animation="slideInDown" iterationCount={3} direction="alternate">Up and down you go
            </Animatable.Text>
            <Animatable.Text animation="zoomInUp" >Zoom me up, Scotty</Animatable.Text>
            <Animatable.Text animation="pulse" easing="ease-in-expo" iterationCount="infinite" style={{ textAlign: 'center' }}>❤️</Animatable.Text>
            <Divider style={{marginVertical:20}}/>
            <View>
                <Card elevation={2} mode='elevated'>
                    <Card.Title title="Card Title" subtitle="Card Subtitle" left={LeftContent} />
                    <Card.Content>
                    <Title>Card title</Title>
                    <Paragraph>Card content</Paragraph>
                    </Card.Content>
                    <Card.Cover source={{ uri: 'https://picsum.photos/700' }} />
                    <Card.Actions>
                    <Button>Cancel</Button>
                    <Button onPress={() => alert('Pressed')}>Ok</Button>
                    </Card.Actions>                   
                </Card>
            </View>
            </ScrollView>
        </View>
    )
}

export default EditAccount

const styles = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 50
  },
  text:{
      color:Colors.blue300,
      fontSize:25
  },
  fab: {
    position: 'relative',
    margin: 16,
    right: 0,
    bottom: 0,
  },
})
