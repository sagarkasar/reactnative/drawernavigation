import { StyleSheet, Text, View, Button } from 'react-native'
import React, { useRef, useState, useEffect, Alert} from 'react';
import { Camera, requestMicrophonePermissionsAsync } from 'expo-camera';
import { Video } from 'expo-av';
import * as MediaLibrary from 'expo-media-library';

const VideoScreen = () => {
    const video = useRef(null);
    const [status, setStatus] = useState({});
    const [hasCameraPermission, sethasCameraPermission] = useState(null);
    const [hasAudioPermission, sethasAudioPermission] = useState(null);
    const [camera, setCamera] = useState(null);
    const [record, setRecord] = useState(null)
    const [type, setType] = useState(Camera.Constants.Type.back);

     useEffect(()=>{
        (async ()=>{
            const CameraStatus = await Camera.requestCameraPermissionsAsync();
            sethasCameraPermission(CameraStatus.status === 'granted');

            const audioStatus = await requestMicrophonePermissionsAsync();
            sethasAudioPermission(audioStatus.status === 'granted');
        })();
    }, []);


    const takeVideo = async () =>{
        if(camera){
            const data = await camera.recordAsync({
                maxDuration: 10
            })
            setRecord(data.uri);
            let uriString = data.uri.toString();
            handleSaveFile(uriString)
        }
    }
    const handleSaveFile = async (uriString) => {
        const {status} = await Camera.requestCameraPermissionsAsync()
        if(status === 'granted'){
             const assert = await MediaLibrary.createAssetAsync(uriString);
             await MediaLibrary.createAlbumAsync('Supra', assert);
        }
        else{
             console.log("Please Check Permissions");
        }
    }
    const stopVideo = async () =>{
        camera.stopRecording();
    }
   if (hasCameraPermission === null || hasAudioPermission === null) {
        return <Text>Null</Text>
    }
    if(hasCameraPermission === false || hasAudioPermission === false){
        return<Text>No camera Access</Text>
    }
  return (
    <View style={styles.cameracontainer}>
            <Camera 
                ref = {ref => setCamera(ref)}
                style={styles.cameracontainer}
                type={type}
                ration={'full'}
                flashMode ={'on'} 
                zoom = {0}
                whiteBalance =  {Camera.Constants.WhiteBalance.auto}
                focusDepth = {2}
            />
            <Video
                ref={video}
                style={styles.video}
                source={{uri: record}}
                useNativeControls
                resizeMode='cover'
                isLooping
                onPlaybackStatusUpdate={status => setStatus(()=>status)}
            />
            <View style={{flexDirection:"row"}}>
            <Button 
                title='flip camera' 
                onPress={()=>{setType(type === Camera.Constants.Type.back ? Camera.Constants.Type.front : Camera.Constants.Type.back )}}
            />
            <Button 
                title= {status.isPlying ?'Pause': 'Play'} 
                onPress={() => status.isPlying ? video.current.pauseAsync() : video.current.playAsync() }
            />
            <Button title='Take video' onPress={()=>takeVideo()} />
            <Button title='Stop video' onPress={()=>stopVideo()} />
            </View>
            <View style={{marginBottom: 50}}></View>
    </View>
  )
}

export default VideoScreen

const styles = StyleSheet.create({
    cameracontainer:{
        flex: 1,
    },
    video:{
        alignSelf: 'center',
        height: 270,
        width: 350
    }
})