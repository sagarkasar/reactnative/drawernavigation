import { StyleSheet, Text, View, ScrollView, Switch, ImageBackground, Alert, Image } from 'react-native'
import React, {useState} from 'react'
import img from '../Assets/Photos/2.jpg'

const FlexBoxDesign = () => {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);
    console.log('value '+ isEnabled);
    Alert.alert("Value "+ isEnabled);
  return (
    
    <View style={styles.container}>
      <View style={{flexDirection:'row', justifyContent:'center'}}>
      <Text style={styles.witchtxt}>Deactive</Text>
        <Switch
          trackColor={{ false: "#767577", true: "#81b0ff" }}
          thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
          onValueChange={toggleSwitch}
          value={isEnabled}
        />
        <Text style={styles.witchtxt}>Active</Text>
      </View>
      <View style={{flexDirection:'row'}}>
          <ImageBackground 
            source={require('../Assets/Photos/2.jpg')} 
            resizeMode='stretch' 
            style={styles.image}
          >
              <Text style={[styles.text, {marginTop: 80}]}>Hello Everyone</Text>
              <Text style={styles.text}>Good Morning</Text>
          </ImageBackground>
          <Image
            source={require('../Assets/Photos/2.jpg')}
            style={styles.image}
            resizeMode='center' 
          />
        
      </View>
      <Text style={styles.maintxt}>FlexBoxDesign</Text>
      <View style={[styles.viewarea, /* {justifyContent:'space-evenly'} */ ]}>
            <Text style={[styles.txt,/* {alignSelf: 'flex-start'} */]}>Hello 1</Text>
            <Text style={[styles.txt, /* {alignSelf:'center'} */]}>Hello 2</Text>
            <Text style={[styles.txt,/* {alignSelf: 'flex-end'} */]}>Hello 3</Text>
      </View>
     
      <View style={{marginBottom: 50}}></View>
    </View>
   
  )
}

export default FlexBoxDesign

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    maintxt:{
        fontSize: 22,
        color: 'red',
        padding: 10,
        margin: 10,
        fontFamily:'serif'
    },
    viewarea:{
        flex: 1,
       
        // flexDirection: 'row',
        // flexDirection: 'column',
        // justifyContent:'center',
        // justifyContent:'flex-end',
        // justifyContent:'space-around',
        // justifyContent:'space-between',
        // justifyContent:'space-evenly',
        // alignItems:'center',
         alignItems: 'flex-end',
        // alignItems: 'stretch',
    },
    txt:{
        fontSize: 18,
        color: 'blue',
        backgroundColor: 'powderblue',
        padding: 5,
        margin: 5,
        fontFamily:'monospace',
        height: 40,
        width: 100,
        flexWrap: 'wrap',
        textAlign:"center"
        // alignSelf: 'flex-start',
        // alignSelf:'baseline',
        // alignSelf: 'center',
        // alignSelf:'flex-end',
        // alignSelf:'stretch'
        
    },
    witchtxt:{
      fontSize: 14,
      marginTop:15
    },
    text:{
      fontSize: 22,
      fontWeight: '700',
      color: 'white',
      alignSelf:'center',
      marginTop: 20
    },
    image:{
      height: 300,
      width: 300,
    }
})