import React, {useState, useEffect} from 'react';
import { Button, StyleSheet, Text, View, Image } from 'react-native';
import { Camera } from 'expo-camera';
import * as MediaLibrary from 'expo-media-library';


const CameraScreen = () => {
    const [hasCameraPermission, sethasCameraPermission] = useState(null);
    const [camera, setCamera] = useState(null);
    const [image, setImage] = useState(null);
    const [type, setType] = useState(Camera.Constants.Type.back);

    useEffect(()=>{
        (async ()=>{
            const CameraStatus = await Camera.requestCameraPermissionsAsync();
            sethasCameraPermission(CameraStatus.status === 'granted');
        })();
    }, []);

    const takePicture = async () =>{
        if (camera){
            const options = { quality: 0.5, base64: true, skipProcessing: true }
            const data = await camera.takePictureAsync(options);
            setImage(data.uri);
            let uriString = data.uri.toString();
            handleSaveFile(uriString)
        }
    }
    
    const handleSaveFile = async (uriString) => {
        const {status} = await Camera.requestCameraPermissionsAsync()
        if(status === 'granted'){
             const assert = await MediaLibrary.createAssetAsync(uriString);
             await MediaLibrary.createAlbumAsync('Supra', assert);
        }
        else{
             console.log("Please Check Permissions");
        }
    }

    if (hasCameraPermission === null) {
        return <Text>null</Text>;
    }
    if(hasCameraPermission === false){
        return <Text>No Camera Access</Text>
    }
    
  return (
    <View style={{flex: 1}}>
        <View style={styles.cameracontainer}>
            <Camera 
                ref = {ref => setCamera(ref)}
                style={styles.cameracontainer}
                type={type}
                ration={'full'}
                flashMode ={'on'} 
                zoom = {0}
                whiteBalance =  {Camera.Constants.WhiteBalance.auto}
                focusDepth = {2}
            />
            <Button 
                title='flip camera' 
                onPress={()=>{setType(type === Camera.Constants.Type.back ? Camera.Constants.Type.front : Camera.Constants.Type.back )}}
            />
            <Button 
                title='Take Picture' 
                onPress={() => takePicture()}
            />
           {/*  {image && <Image source={{uri : image}} style={{ flex:1 }}/>} */}
            <View style={{marginBottom: 50}}></View>
        </View>
    </View>
  )
}

export default CameraScreen

const styles = StyleSheet.create({
    cameracontainer:{
        flex: 1,
    }
})