import React, {useState} from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import axios from 'axios';

const ProductDetails = (props) => {
    const postData = () => {
        axios({
            method: 'post',
            url: 'https://gorest.co.in/public/v1/users',
            data:{
                name: "ABCDRDD",
                email: "ABCDSFSTFDS@gmail.com",
                gender: 'FEMale',
                status: 'inactive'
            },
            headers: {
            "Authorization": "Bearer d484c6e730cc22caf59cce6ee33c6ed1142a56f77dee4ac6b86ea8341492e408",
            "Accept": "application/json",
            "Content-Type": "application/json"
            }
        })
        .then(function (response) {
            console.log("response", JSON.stringify(response.data))
        })
        .catch(function (error) {
            console.log("error", error)
        })
    }
    const updateData = () => {
        axios({
            method: 'put',
            url: 'https://gorest.co.in/public/v1/users/2306',
            data:{
                name: "ABBCCDD",
                email: "ABBCCDD@gmail.com",
                gender: 'FeMale',
                status: 'inactive'
            },
            headers: {
            "Authorization": "Bearer d484c6e730cc22caf59cce6ee33c6ed1142a56f77dee4ac6b86ea8341492e408",
            "Accept": "application/json",
            "Content-Type": "application/json"
            }
        })
        .then(function (response) {
            console.log("response", JSON.stringify(response.data))
        })
        .catch(function (error) {
            console.log("error", error)
        })
    }
    const getData = () => {
        axios({
            method: 'get',
            url: 'https://gorest.co.in/public/v1/users',
            headers: {
            "Authorization": "Bearer d484c6e730cc22caf59cce6ee33c6ed1142a56f77dee4ac6b86ea8341492e408",
            "Accept": "application/json",
            "Content-Type": "application/json"
            }
        })
        .then(function (response) {
            console.log("response", JSON.stringify(response.data))
        })
        .catch(function (error) {
            console.log("error", error)
        })
    }
    const deleteData = () => {
        axios({
            method: 'delete',
            url: 'https://gorest.co.in/public/v1/users/1879',
            headers: {
            "Authorization": "Bearer d484c6e730cc22caf59cce6ee33c6ed1142a56f77dee4ac6b86ea8341492e408",
            "Accept": "application/json",
            "Content-Type": "application/json"
            }
        })
        .then(function (response) {
            console.log("response", JSON.stringify(response.data))
        })
        .catch(function (error) {
            console.log("error", error)
        })
    }

    const [Textvalue, setTextValue] = useState("welcome to supra");

    const sagar = () =>{
        setTextValue("Visit Again")
    }
    const [Hide, setHide] = useState(true);
    const onClick = () => { 
        setHide(!Hide);
    }
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Name: {props.name}</Text>
            <Text style={styles.text}>Email: {props.email}</Text>
            <Text style={styles.text}>Address: {props.address}</Text>
            <Text style={styles.text}>Phone: {props.phone}</Text>

            <Text style={styles.text}>{Textvalue}</Text>
            <TouchableOpacity>
                <Text style={styles.btn} onPress={()=> sagar()}>Click Here</Text>
            </TouchableOpacity>
            { Hide?
            (<TouchableOpacity>
                <Text style={styles.btn} onPress={()=> onClick()}>Hide</Text>
            </TouchableOpacity>)
            :
            (<TouchableOpacity>
                <Text style={styles.btn} onPress={()=> onClick()}>Show</Text>
            </TouchableOpacity>)
            }
           {/*  <Text style={styles.text}>Product Details Page</Text>
            <Text style={styles.text}>api Practice</Text>
             
            <TouchableOpacity>
                <Text style={styles.btn} onPress={()=> getData()}>Show(Get) Data</Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={styles.btn} onPress={()=> postData()}>Insert(Post) Data</Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={styles.btn} onPress={()=> updateData()}>Update(Put) Data</Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={styles.btn} onPress={()=> deleteData()}>Delete(Delete) Data</Text>
            </TouchableOpacity> */}
        </View>
    )
}

export default ProductDetails;

const styles = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor: '#fff',
    
    alignItems:'center'
  },
  text:{
      color:'red',
      fontSize:25,
      fontWeight:'700'
  },
  btn:{
      color: 'black',
      backgroundColor: 'gray',
      height: 50,
      width: 300,
      borderRadius: 30,
      borderWidth: 1,
      borderColor: 'red',
      margin:10,
      fontSize: 23,
      padding: 5,
      textAlign: 'center',

  }
})
