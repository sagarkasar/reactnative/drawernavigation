
export default {
    HOME: "Home",
    PROFILE: "Profile",
    ACCOUNT: "Account",
    PRODUCTDETAILS: 'ProductDetails',
    EDITPROFILE: 'EditProfile',
    EDITACCOUNT: 'EditAccount',
    TAB: 'Tabs',
    DRAWER: 'Drawer',
    PROFILENavigation: 'ProfileNavigation'
}