import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NavigationStrings from "../Constants/NavigationStrings";
import Account from "../Screens/Account";
import EditAccount from "../Screens/EditAccount";
import EditProfile from "../Screens/EditProfile";
import ModelPopup from "../Screens/ModelPopup";
import Flatlist from "../Screens/FlatList";
import CameraScreen from "../Screens/CameraScreen";
import VideoScreen from "../Screens/VideoScreen";
import NotificationScreen from "../Screens/Notification";

const  Stack= createNativeStackNavigator();

export default function AccountStack(){
    return(
        <Stack.Navigator
        screenOptions={
           {headerShown:false}
        }
        >
            <Stack.Screen name={NavigationStrings.ACCOUNT} component={Account}/>
            <Stack.Screen name={NavigationStrings.EDITACCOUNT} component={EditAccount}/>
            <Stack.Screen name={NavigationStrings.EDITPROFILE} component={EditProfile}/>
            <Stack.Screen name={"ModelPopup"} component={ModelPopup}/>
            <Stack.Screen name={"FlatList"} component={Flatlist}/>
            <Stack.Screen name={"Camera"} component={CameraScreen}/>
            <Stack.Screen name={"Video"} component={VideoScreen}/>
            <Stack.Screen name={"Notification"} component={NotificationScreen}/>

        </Stack.Navigator>
    )
}