import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Home from "../Screens/Home";
import Account from '../Screens/Account';
import Profile from '../Screens/Profile';
import EditProfile from '../Screens/EditProfile';
import ModelPopup from '../Screens/ModelPopup';
import Flatlist from '../Screens/FlatList';
import FlexBoxDesign from '../Screens/FlexBoxDesign';
const Tab = createMaterialTopTabNavigator();
const TopTab = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="FlexBoxDesign" component={FlexBoxDesign} />
      <Tab.Screen name="EditProfile" component={EditProfile} />
      <Tab.Screen name="ModelPopup" component={ModelPopup} />
      <Tab.Screen name="Flatlist" component={Flatlist} />
    </Tab.Navigator>
  )
}

export default TopTab

const styles = StyleSheet.create({

})