import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NavigationStrings from "../Constants/NavigationStrings";
import Home from "../Screens/Home";
import ProductDetails from "../Screens/ProductDetails";

const  Stack= createNativeStackNavigator();

export default function HomeStack(){
    return(
        <Stack.Navigator 
        screenOptions={
           {headerShown:false}
        }
        >
            <Stack.Screen name={NavigationStrings.HOME} component={Home}/>
            <Stack.Screen name={NavigationStrings.PRODUCTDETAILS} component={ProductDetails}/>
        </Stack.Navigator>
    )
}