import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Entypo from "react-native-vector-icons/Entypo";
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import NavigationStrings from "../Constants/NavigationStrings";
import HomeStack from "./HomeStack";
import ProfileStack from "./ProfileStack";
import AccountStack from "./AccountStack";

const Tab = createBottomTabNavigator();

function TabRoutes() {
  return (
   
      <Tab.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerShown:false,
        tabBarActiveTintColor:'red',
        tabBarInactiveTintColor:'gray',
        tabBarShowLabel:false,
        tabBarStyle:{
          position:'absolute',
          backgroundColor: '#defcf1',
        },
        
      }}
      >
        <Tab.Screen  
          name={'HomeStack'}
          component={HomeStack}
          options={{
              tabBarIcon: ({ focused }) => (
                <Entypo
                  name='home'
                  size={30}
                  style={{ marginTop: 15}}
                />
              ),  
            }}
        />
        <Tab.Screen 
        name={"ProfileStack"}
        component={ProfileStack}
        options={{
              tabBarIcon: ({ focused }) => (
                <AntDesign
                  name='profile'
                  size={35}
                  style={{ marginTop: 15 }}
                />
              ),
            }}
        />
        <Tab.Screen 
        name={"AccountStack"}
        component={AccountStack}
        options={{
              tabBarIcon: ({ focused }) => (
                <MaterialCommunityIcons
                  name='account'
                  size={35}
                  style={{ marginTop: 15 }}
                />
              ),
            }}
        />
      </Tab.Navigator>
   
  );
}

export default TabRoutes;