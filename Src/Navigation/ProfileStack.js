import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NavigationStrings from "../Constants/NavigationStrings";
import Profile from "../Screens/Profile";
import EditProfile from "../Screens/EditProfile";
import FlexBoxDesign from '../Screens/FlexBoxDesign'
import TabBarScreen from "../Screens/TabBarScreen";

const  Stack= createNativeStackNavigator();

export default function ProfileStack(){
    return(
        <Stack.Navigator
        screenOptions={
           {headerShown:false}
        }
        >
            <Stack.Screen name={NavigationStrings.PROFILE} component={Profile}/>
            <Stack.Screen name={NavigationStrings.EDITPROFILE} component={EditProfile}/>
            <Stack.Screen name='FlexBoxDesign' component={FlexBoxDesign}/>
            <Stack.Screen name='TabBarScreen' component={TabBarScreen}/>
        </Stack.Navigator>
    )
}