import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import NavigationStrings from './Src/Constants/NavigationStrings';
import AccountStack from './Src/Navigation/AccountStack';
import HomeStack from './Src/Navigation/HomeStack';
import ProfileStack from './Src/Navigation/ProfileStack';
import TabRoutes from './Src/Navigation/TabRoutes';
import Account from './Src/Screens/Account';
import Home from './Src/Screens/Home';
import Profile from './Src/Screens/Profile';
import TopTab from './Src/Navigation/TopTab';
const Stack= createNativeStackNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  return (
    
      <NavigationContainer>
          <Drawer.Navigator /*  screenOptions={
           {headerShown:false}
        } */
          
          >
            <Drawer.Screen name="Welcome" component={TabRoutes} />
            <Drawer.Screen name={NavigationStrings.HOME} component={HomeStack} />
            <Drawer.Screen name={NavigationStrings.PROFILE} component={ProfileStack} />
          </Drawer.Navigator>
       
      </NavigationContainer>
   
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
